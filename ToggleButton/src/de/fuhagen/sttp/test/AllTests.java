package de.fuhagen.sttp.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({FlatToggleButtonTest.class, FlatTrippleButtonTest.class})
public class AllTests {

}
