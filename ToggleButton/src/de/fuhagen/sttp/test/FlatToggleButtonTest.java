/**
 * 
 */
package de.fuhagen.sttp.test;

import static org.junit.Assert.assertTrue;

import javax.swing.Action;

import org.junit.Test;

import de.fuhagen.sttp.demo.DemoAction;
import de.fuhagen.sttp.gui.FlatToggleButton;

/**
 * @author thomas
 *
 */
public class FlatToggleButtonTest {

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatToggleButton#FlatToggleButton(javax.swing.AbstractAction, javax.swing.AbstractAction, int, int)}.
     */
    @Test
    public void testFlatToggleButton() {
        Action on = new DemoAction("on");
        Action off = new DemoAction("off");
        FlatToggleButton button = new FlatToggleButton(on, off, 20, 80);

        assertTrue("width", button.getWidth() == 20);
        assertTrue("width", button.getHeight() == 80);
        assertTrue("action", button.getAction() == on);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatToggleButton#getOnAction()}.
     */
    @Test
    public void testGetOnAction() {
        Action on = new DemoAction("on");
        Action off = new DemoAction("off");
        FlatToggleButton button = new FlatToggleButton(on, off, 20, 80);

        assertTrue("on action", button.getOnAction() == on);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatToggleButton#getOffAction()}.
     */
    @Test
    public void testGetOffAction() {
        Action on = new DemoAction("on");
        Action off = new DemoAction("off");
        FlatToggleButton button = new FlatToggleButton(on, off, 20, 80);

        assertTrue("off action", button.getOffAction() == off);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatToggleButton#isOn()}.
     */
    @Test
    public void testIsOn() {
        Action on = new DemoAction("on");
        Action off = new DemoAction("off");
        FlatToggleButton button = new FlatToggleButton(on, off, 20, 80);

        assertTrue("is on", !button.isOn());
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatToggleButton#setState(boolean)}.
     */
    @Test
    public void testSetOn() {
        Action on = new DemoAction("on");
        Action off = new DemoAction("off");
        FlatToggleButton button = new FlatToggleButton(on, off, 20, 80);

        button.setState(true);
        assertTrue("is on", button.isOn());

        button.setState(false);
        assertTrue("is off", !button.isOn());
    }

}
