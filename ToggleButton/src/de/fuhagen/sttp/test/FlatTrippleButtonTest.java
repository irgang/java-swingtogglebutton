/**
 * 
 */
package de.fuhagen.sttp.test;

import static org.junit.Assert.assertTrue;

import javax.swing.Action;

import org.junit.Test;

import de.fuhagen.sttp.demo.DemoAction;
import de.fuhagen.sttp.gui.FlatTrippleButton;
import de.fuhagen.sttp.gui.FlatTrippleButton.STATE;

/**
 * @author thomas
 *
 */
public class FlatTrippleButtonTest {

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatTrippleButton#FlatTrippleButton(javax.swing.Action, javax.swing.Action, javax.swing.Action, int, int)}.
     */
    @Test
    public void testFlatTrippleButton() {
        Action on = new DemoAction("on");
        Action middle = new DemoAction("middle");
        Action off = new DemoAction("off");
        FlatTrippleButton button = new FlatTrippleButton(on, off, middle, 20,
                80);

        assertTrue("width", button.getWidth() == 20);
        assertTrue("width", button.getHeight() == 80);
        assertTrue("action", button.getAction() == middle);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatTrippleButton#getOnAction()}.
     */
    @Test
    public void testGetOnAction() {
        Action on = new DemoAction("on");
        Action middle = new DemoAction("middle");
        Action off = new DemoAction("off");
        FlatTrippleButton button = new FlatTrippleButton(on, off, middle, 20,
                80);

        assertTrue("on action", button.getOnAction() == on);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatTrippleButton#getOffAction()}.
     */
    @Test
    public void testGetOffAction() {
        Action on = new DemoAction("on");
        Action middle = new DemoAction("middle");
        Action off = new DemoAction("off");
        FlatTrippleButton button = new FlatTrippleButton(on, off, middle, 20,
                80);

        assertTrue("off action", button.getOffAction() == off);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatTrippleButton#getMiddleAction()}.
     */
    @Test
    public void testGetMiddleAction() {
        Action on = new DemoAction("on");
        Action middle = new DemoAction("middle");
        Action off = new DemoAction("off");
        FlatTrippleButton button = new FlatTrippleButton(on, off, middle, 20,
                80);

        assertTrue("middle action", button.getMiddleAction() == middle);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatTrippleButton#getState()}.
     */
    @Test
    public void testGetState() {
        Action on = new DemoAction("on");
        Action middle = new DemoAction("middle");
        Action off = new DemoAction("off");
        FlatTrippleButton button = new FlatTrippleButton(on, off, middle, 20,
                80);

        assertTrue("state", button.getState() == STATE.OFF);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.FlatTrippleButton#setState(de.fuhagen.sttp.gui.FlatTrippleButton.STATE)}.
     */
    @Test
    public void testSetState() {
        Action on = new DemoAction("on");
        Action middle = new DemoAction("middle");
        Action off = new DemoAction("off");
        FlatTrippleButton button = new FlatTrippleButton(on, off, middle, 20,
                80);

        button.setState(STATE.ON);
        assertTrue("on state", button.getState() == STATE.ON);
        button.setState(STATE.MIDDLE);
        assertTrue("middle state", button.getState() == STATE.MIDDLE);
        button.setState(STATE.OFF);
        assertTrue("off state", button.getState() == STATE.OFF);
    }
}
