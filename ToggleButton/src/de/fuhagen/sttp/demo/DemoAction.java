package de.fuhagen.sttp.demo;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * Demo action for {@link ToggleButtonDemo}.
 * 
 * @author thomas
 *
 */
public class DemoAction extends AbstractAction {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1990080725055658590L;
    /**
     * Name of this action.
     */
    private String            name             = "";

    /**
     * This constructor builds a new demo action.
     * 
     * @param actionName
     *      name / short description of action
     */
    public DemoAction(String actionName) {
        super();

        name = actionName;
        putValue(Action.SHORT_DESCRIPTION, actionName);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(name + " action fired");
    }
}
