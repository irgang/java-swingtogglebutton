package de.fuhagen.sttp.demo;

import java.awt.FlowLayout;

import javax.swing.JFrame;

import de.fuhagen.sttp.gui.FlatToggleButton;
import de.fuhagen.sttp.gui.FlatTrippleButton;

/**
 * Demo of {@link FlatToggleButton} and {@link FlatTrippleButton} GUI elements.
 * 
 * @author thomas
 *
 */
public class ToggleButtonDemo extends JFrame {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 683933609536453500L;

    /**
     * This method create a new {@link ZoomSlider} demo frame.
     * 
     * @param args
     */
    public static void main(String[] args) {
        new ToggleButtonDemo();
    }

    /**
     * This constructor builds a JFrame with a {@link FlatToggleButton}.
     */
    public ToggleButtonDemo() {
        super("Flat toggle button demo");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.LEFT));

        DemoAction onAction = new DemoAction("On");
        DemoAction offAction = new DemoAction("Off");

        FlatToggleButton toggleButton = new FlatToggleButton(onAction,
                offAction, 30, 80);
        add(toggleButton);

        DemoAction middleAction = new DemoAction("Middle");

        FlatTrippleButton trippleButton = new FlatTrippleButton(onAction,
                offAction, middleAction, 30, 80);
        add(trippleButton);

        setSize(200, 200);
        setVisible(true);
    }
}
